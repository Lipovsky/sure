# Sure-Stack

К `ExecutionContext` прилагается реализация стека

(в виде отдельной библиотеки [Sure-Stack](http://gitlab.com/Lipovsky/sure-stack)).

## Стеки

Предлагаются две реализации:

### `GuardedMmapStack`

Заголовок: `sure/stack/mmap.hpp` \
Пространство имен: `sure::stack::`

Диапазон страниц, аллоцированный с помощью [mmap](https://man7.org/linux/man-pages/man2/mmap.2.html) с установленной guard page.

Новый стек аллоцируется с помощью статического конструктора `AllocateBytes(size_t at_least)`:

```cpp
auto stack = GuardedMmapStack::AllocateBytes(at_least);
```

Для стека будет установлен guard page.

Стек (без учета guard page) будет иметь размер по крайней мере `at_least` байт.

Экземпляр `GuardedMmapStack` владеет выделенной памятью, при разрушении он возвращает ее операционной системе.

Метод `MutView` возвращает диапазон адресов стека в виде `std::span<std::byte>`.

### `NewStack`

Заголовок: `sure/stack/new.hpp` \
Пространство имен: `sure::stack::`

Простая динамическая аллокация, `new std::byte[]`

Новый стек аллоцируется с помощью статического конструктора `AllocateBytes(size_t at_least)`:

```cpp
auto stack = NewStack::AllocateBytes(at_least);
```

Стек будет иметь размер по крайней мере `at_least` байт.

Экземпляр `NewStack` владеет выделенной памятью, при разрушении он возвращает ее аллокатору.

Метод `MutView` возвращает диапазон адресов стека в виде `std::span<std::byte>`.
