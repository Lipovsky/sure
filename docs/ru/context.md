# Sure

Библиотека предоставляет механизм кооперативного переключения контекста исполнения (нелокальной передачи управления) для реализации stackful корутин и файберов.

## `ExecutionContext`

Заголовок: [`sure/context.hpp`](/source/sure/context.hpp)

Сохраненный контекст остановленного исполнения представлен классом [`ExecutionContext`](/source/sure/context.hpp) и включает:

- Регистры процессора
- Контекст санитайзера
- Контекст исключения

### Операции

- _Установка_ (`Setup`) и
- _Переключение_ (`SwitchTo` / `ExitTo`) контекста.

#### Переключение контекста

Выглядит так: `current.SwitchTo(target)`, где `current` и `target` – `ExecutionContext`.

Контекст текущего исполнения будет сохранен в `current`, после чего активируется контекст `target`.

Активируемый контекст `target` 
- либо был сохранен ранее с помощью `SwitchTo` (стоял слева от вызова, в позиции `this`),
- либо был установлен "вручную" с помощью `Setup`.

##### Пустой контекст

После `SwitchTo` ранее установленный / сохраненный в `target` контекст исполнения активируется на процессоре, а сам `target` становится _пустым_.

Переключение на пустой `ExecutionContext` – UB.

#### Установка контекста

Метод `Setup` конструирует **новое исполнение**.  

Метод принимает два аргумента:
- `stack` – диапазон памяти (`StackView` = `std::span<std::byte>`), который будет служить стеком для нового исполнения 
- `trampoline` – указатель на реализацию интерфейса [`ITrampoline`](/source/sure/trampoline.hpp) с единственным методом `Run`.

После первого переключения через `SwitchTo` в установленный через `Setup` контекст на стеке `stack` начнется вызов `trampoline->Run()`.

#### `StackView`

`ExecutionContext` в `Setup` получает **view** на стек ([`StackView`](/source/sure/stack_view.hpp)), что означает: контекст не владеет стеком, не управляет его памятью. 

Аллокация стека и управление памятью – ответственность пользователя `ExecutionContext`.

#### `ITrampoline`

Заголовок: [`sure/trampoline.hpp`](/source/sure/trampoline.hpp)

`ITrampoline` представляет для `ExecutionContext` исполняемую сущность (например, файбер или корутину) с методом `Run`.

##### `[[noreturn]]`

Вызов `Run` не должен возвращать управление caller-у (т.е. завершаться обычным образом), он должен быть остановлен (навсегда) вызовом `ExitTo` на связанном `ExecutionContext`.

#### `ExitTo`

При последнем переключении из контекста (т.е. в конце `ITrampoline::Run` корутины / файбера) нужно использовать `ExitTo` вместо `SwitchTo`.

Этот граничный случай необходим для корректной работы проверок в санитайзерах.

Активировать контекст после переключения из него с помощью `ExitTo` нельзя.

### Pin

`ExecutionContext` не поддерживает копирование и перемещение.