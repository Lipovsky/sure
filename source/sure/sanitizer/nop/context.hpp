#pragma once

#include <sure/stack_view.hpp>
#include <sure/trampoline.hpp>

#include <sure/unreachable.hpp>

namespace sure {

template <typename MachineContext>
class WithSanitizer {
  using Context = WithSanitizer;

 public:
  WithSanitizer() = default;

  void Setup(StackView stack, ITrampoline* trampoline) {
    underlying_.Setup(stack, trampoline);
  }

  void SwitchTo(Context& target) {
    underlying_.SwitchTo(target.underlying_);
  }

  [[noreturn]] void ExitTo(Context& target) {
    SwitchTo(target);
    Unreachable();
  }

  void* StackPointer() const noexcept {
    return underlying_.StackPointer();
  }

 private:
  MachineContext underlying_;
};

}  // namespace sure
