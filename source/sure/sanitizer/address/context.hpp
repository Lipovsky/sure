#pragma once

#include <sure/stack_view.hpp>
#include <sure/trampoline.hpp>

#include <sure/unreachable.hpp>

#include <sanitizer/asan_interface.h>

namespace sure {

template <typename MachineContext>
class WithSanitizer final : private ITrampoline {
  using Context = WithSanitizer;

 public:
  WithSanitizer() = default;

  void Setup(StackView stack, ITrampoline* trampoline) {
    user_ = trampoline;

    underlying_.Setup(stack, this);

    {
      stack_ = stack.data();
      stack_size_ = stack.size();
    }
  }

  void SwitchTo(Context& target) {
    BeforeSwitchTo(target);

    underlying_.SwitchTo(target.underlying_);

    AfterSwitch();
  }

  [[noreturn]] void ExitTo(Context& target) {
    BeforeExitTo(target);

    underlying_.SwitchTo(target.underlying_);

    Unreachable();
  }

  void* StackPointer() const noexcept {
    return underlying_.StackPointer();
  }

 private:
  // ITrampoline
  void Run() noexcept override {
    AfterStart();
    user_->Run();
  }

  void AfterStart() {
    __sanitizer_finish_switch_fiber(nullptr, &(from_->stack_),
                                    &(from_->stack_size_));
  }

  void BeforeSwitchTo(Context& target) {
    target.from_ = this;
    __sanitizer_start_switch_fiber(&fake_stack_, target.stack_,
                                   target.stack_size_);
  }

  void AfterSwitch() {
    __sanitizer_finish_switch_fiber(fake_stack_, &(from_->stack_),
                                    &(from_->stack_size_));
  }

  void BeforeExitTo(Context& target) {
    target.from_ = this;
    // https://github.com/llvm-mirror/compiler-rt/blob/69445f095c22aac2388f939bedebf224a6efcdaf/include/sanitizer/common_interface_defs.h#L299
    __sanitizer_start_switch_fiber(nullptr, target.stack_, target.stack_size_);
  }

 private:
  ITrampoline* user_ = nullptr;

  MachineContext underlying_;

  // Address Sanitizer
  const void* stack_;
  size_t stack_size_;
  void* fake_stack_;
  Context* from_;
};

}  // namespace sure
