#pragma once

#include <sure/trampoline.hpp>
#include <sure/stack_view.hpp>

#include <sure/machine/context.hpp>
#include <sure/sanitizer/context.hpp>
#include <sure/exceptions/context.hpp>

#include <sure/build.hpp>

#include <sure/unreachable.hpp>

#include <utility>

namespace sure {

// Execution Context =
// 1) Machine context (registers) +
// 2) [Address | Thread] sanitizer context +
// 3) Exceptions context

class ExecutionContext {
  // clang-format off

  // NB: Order matters!
  using Impl =
      WithExceptions<
          WithSanitizer<
              MachineContext>>;

  // clang-format on

 public:
  // Empty context, cannot be a target for SwitchTo
  ExecutionContext() = default;

  // Non-copyable
  ExecutionContext(const ExecutionContext&) = delete;
  ExecutionContext& operator=(const ExecutionContext&) = delete;

  // Non-movable
  ExecutionContext(ExecutionContext&&) = delete;
  ExecutionContext& operator=(ExecutionContext&&) = delete;

  // Prepare execution context for running trampoline->Run()
  // on stack `stack`
  void Setup(StackView stack, ITrampoline* trampoline) {
    impl_.Setup(stack, trampoline);
  }

  // Symmetric Control Transfer
  // 1) Save current execution context to `this`
  // 2) Activate `target` context
  void SwitchTo(ExecutionContext& target) {
    impl_.SwitchTo(target.impl_);
  }

  // Leave current execution context forever
  // Last context switch in ITrampoline::Run
  [[noreturn]] void ExitTo(ExecutionContext& target) {
    impl_.ExitTo(target.impl_);
  }

  // For overflow checking
  // Calling StackPointer on non-suspended ExecutionContext results in UB
  void* StackPointer() const noexcept {
    return impl_.StackPointer();
  }

 private:
  Impl impl_;
};

}  // namespace sure
