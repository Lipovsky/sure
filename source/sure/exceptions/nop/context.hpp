#pragma once

namespace sure {

template <typename WithSanitizerContext>
using WithExceptions = WithSanitizerContext;

}  // namespace sure
